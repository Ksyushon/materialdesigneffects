package com.firststeps.degtyar.materialdesigneffects;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;

public class NestedToolbarActivity extends AppCompatActivity {

    Toolbar mToolbar1, mToolbar2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nested_toolbar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        mToolbar1 = (Toolbar) findViewById(R.id.nested_toolbar_1);
        mToolbar2 = (Toolbar) findViewById(R.id.nested_toolbar_2);

        final FrameLayout frameLayout = (FrameLayout) findViewById(R.id.nested_parentframe);

        setSupportActionBar(mToolbar2);
        getSupportActionBar().setTitle("Title");

        mToolbar1.setNavigationIcon(R.drawable.ic_action_menu);
        mToolbar1.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(frameLayout, "Tapped Nav Drawer", Snackbar.LENGTH_SHORT).show();
            }
        });

        mToolbar2.setTitleTextColor(getResources().getColor(android.R.color.tertiary_text_light));
    }
}
